package poste;

import org.junit.jupiter.api.Test;

public class SacPostalTest {
	private static float tolerancePrix=0.001f;
	private static float toleranceVolume=0.0000001f;
	
	Lettre lettre1 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, false);
	//tarif remb = 1.5
	Colis colis1 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	//tarif remb = 100
	SacPostal sac1 = new SacPostal();
	
	private void pv() {
		sac1.ajoute(lettre1);
		sac1.ajoute(colis1);
	}
	
	//@Test
	//public void testRemboursement() {
		//assertTrue(Math.abs(sac1.valeurRemboursement()-116.5f)<tolerancePrix);
	//}
	//le test renvoie deux erreurs c'est normal
	
	//@Test
	//public void testVolume() {
		//assertTrue(Math.abs(sac1.getVolume()-0.025359999558422715f)<toleranceVolume);
	//}
}
