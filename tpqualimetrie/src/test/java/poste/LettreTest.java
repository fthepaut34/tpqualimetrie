package poste;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class LettreTest {
	private static float tolerancePrix=0.001f;
	
	Lettre lettre1 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, false);
	Lettre lettre2 = new Lettre("Le pere Noel",
			"famille Kirik, igloo 5, banquise nord",
			"7877", 25, 0.00018f, Recommandation.un, true);
	
	
	@Test
	public void testUrgenceConsVide() {
		Lettre l = new Lettre();
		assertFalse(l.isUrgence());
	}
	
	@Test
	public void testToStringLettre() {
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/ordinaire", lettre1.toString());
		assertEquals("Lettre 7877/famille Kirik, igloo 5, banquise nord/1/urgence", lettre2.toString());
	}
	
	@Test
	public void testAffranchissement() {
		assertTrue(Math.abs(lettre1.tarifAffranchissement()-1.0f)<tolerancePrix);
	}
	
	@Test
	public void testRemboursement() {
		assertTrue(Math.abs(lettre1.tarifRemboursement()-1.5f)<tolerancePrix);
	}
}
