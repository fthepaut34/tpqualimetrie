package poste;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ColisExpressTest {
	
	@Test
	public void testColisExpressInvalide() {
		try {
			ColisExpress colisPlus30 = new ColisExpress("Montpellier", "Marseille", "13000", 35, 10, Recommandation.zero, "contenu", 50, false);
			assertTrue(false);
		}
		catch(ColisExpressInvalide e) {
			assertTrue(true);
		}
	}
}