package poste;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

public class ColisTest {
	private static float tolerancePrix=0.001f;
	
	Colis colis1 = new Colis("Le pere Noel", 
			"famille Kaya, igloo 10, terres ouest",
			"7877", 1024, 0.02f, Recommandation.deux, "train electrique", 200);
	
	@Test
	public void testToStringColis() {
		assertEquals("Colis 7877/famille Kaya, igloo 10, terres ouest/2/0.02/200.0", colis1.toString());
	}
	
	@Test
	public void testAffranchissement() {
		assertTrue(Math.abs(colis1.tarifAffranchissement()-3.5f)<tolerancePrix);
	}
	
	@Test
	public void testRemboursement() {
		assertTrue(Math.abs(colis1.tarifRemboursement()-100.0f)<tolerancePrix);
	}
}
